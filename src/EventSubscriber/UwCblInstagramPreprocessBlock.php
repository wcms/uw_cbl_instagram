<?php

namespace Drupal\uw_cbl_instagram\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblInstagramPreprocessBlock.
 */
class UwCblInstagramPreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with Instagram and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_instagram')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content')['#block_content'];

      // Variable for Instagram.
      $instagram['url'] = $block->field_uw_ing_url->value;

      // Set the Instagram in variables.
      $variables->set('instagram', $instagram);
    }
  }
}
